.. LearningSys documentation master file, created by
   sphinx-quickstart on Mon Jan  6 00:32:15 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   .. toctree::
  ..  :maxdepth: 2
  ..  :caption: Contents:


Welcome to LearningSys's documentation!
=======================================


.. toctree::
   :maxdepth: 2

   example

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
